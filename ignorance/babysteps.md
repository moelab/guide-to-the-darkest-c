# 第一步

学习 C 语言的第一步，从来不是快乐的。

C 语言自 20 世纪 70 年代出现以来，提供了够用的抽象层次，又足够贴近硬件，广为使用至今。

C 语言有国际标准，如 C89 C99 C11。但是 C 语言的编译器厂商不一定完全遵循标准，如 GNU 的 GCC 和微软的 MSVC 就存在诸多的不同。为什么他们可以不遵循标准？因为标准往往是滞后的，编译器是与开发者天天打交道的。开发者关心什么，不关心什么，编译器厂商有自己的商业策略和目标。而编译器作为产品，不同的编译器之间是有竞争的，甚至说，差异化的产品才是人们做出不同选择的关键。

可以说，我们使用的 C 语言，主要是使用的是建立在特定的编译器之上的 C 语言的方言。

目前市面上的教程多是采用了 GCC 编译器，这也是本书的选择。GCC 是 GNU 组织提供的编译器，是一款自由软件，是目前跨平台编译器的事实标准。Linux、BSD 等操作系统都采用 GCC 作为编译器。

除了 GCC 以外，你可能听说过微软公司开发的 MSVC，或是苹果公司赞助支持的 Clang+LLVM。这些名词你如果知道自然是好的，便于类比，不知道也无关紧要。厂商们都一定会在最广泛使用的功能上提供支持（这些功能也必然地写在了语言标准里），顶多会出现命名的不同，或者是摆放位置的不同，但是本质上不会有区别。让我们安心地从 GCC 开始，其他方言在你私下用到的时候，再迁移你所学过的知识，不迟。

很多人在第一步时，接触了过时的工具，或者是超前的不稳定的工具，撞得头破血流。

多数人使用的 PC 是 Windows 系统，但是微软提供的 Visual Studio 使用的是 MSVC，这个方言与 GCC 是不同的，而教程大都是面向 GCC 的，这就产生了不一致，你不能很好地在 Visual Studio 里抄写教程里的代码并得到相同的结果。为了避免这个问题，我们最好首先准备一个完整的 GNU/Linux + GCC 的环境。

我不希望在入门阶段向学习者提供过多的选择。在入门时，一个人尚未知道整个事物的全貌，就像认不全词典里的常用字，如果此时告诉他茴字的五种写法，并醉心于把这五个字写的一样好，是极为不妥的。

当学习者使用一种工具、一种方法、一条路径完整地走到了终点，从源码出发得到了可执行程序，此时回过头来，再对任意一处产生好奇心，进而深究拓展，都是值得鼓励的。在这种主线明确的基础上，改动一部分，其改动的边界是明确的，造成的影响是局部的，即使遇到困难也不会令人恐慌或无助。这样的学习能够不断丰富自己的知识体系，是我目前认为理想的学习思路。

如果你已经在使用 Linux 发行版，那么请继续。如果你使用的是 Windows 或是 MacOS，那么请参照[这篇文章][1]，安装虚拟机管理软件。

[1]: ../indie-games/virtual-machine.md
